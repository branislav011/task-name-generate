<form method="post" action="/generateBranchName">
    @csrf
    <!-- ticket number -->
    <div class="row">
        <div class="col-6 m-2">
            <input type="text" placeholder="Ticket number" id="ticketNumber" name="ticketNumber" class="form-control" value="{{ old('ticketNumber') }}" required>
        </div>
    </div>
    <!-- ticket name -->
    <div class="row">
        <div class="col-6 m-2">
            <input type="text" placeholder="Ticket name" id="ticketName" name="ticketName" class="form-control" value="{{ old('ticketName') }}" required>
        </div>
    </div>
    <!-- ticket type -->
    <div class="row">
        <div class="col-6 m-2">
            <select id="type" name="type" class="form-select">
                @foreach($taskTypes as $type)
                    <option value="{{ $type->value }}">{{ ucfirst($type->value) }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <!-- generate command checkbox -->
    <div class="row">
        <div class="col-6 m-2">
            <input type="checkbox" name="createCommand" id="createCommand" class="form-check-input" checked>
            <label for="createCommand">
                Generate Git Command
            </label>
        </div>
    </div>
    <!-- execute command -->
    <div class="row">
        <div class="col-6 m-2">
            <input type="checkbox" name="execute" id="execute" class="form-check-input">
            <label for="execute">
                Execute Command <strong>(New Branch will be created!)</strong>
            </label>
        </div>
    </div>
    <!-- submit -->
    <div class="row">
        <div class="col-6 m-2">
            <button type="submit" class="btn btn-success">Generate</button>
        </div>
    </div>
</form>
