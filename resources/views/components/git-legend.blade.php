<div class="row">
    <div class="col-6 m-2">
        <div class="card">
            <div class="card-header">
                Legend
            </div>
            <div class="card-body">
                <h5 class="card-title">Task types</h5>
                <div>
                    <ul>
                        @foreach($taskTypes as $type)
                            <li>
                                <img src="{{ asset('img/' . $type->value . '.svg') }}" alt="{{ $type->value }}"> {{ ucfirst($type->value) }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
