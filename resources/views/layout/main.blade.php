<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @section('customcss')
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    @show
    @section('customjs')
        <script src="{{ asset('js/app.js') }}" defer></script>
    @show
    <title>Git Command Generator</title>
</head>
<body>
    <div>
        @yield("content")
    </div>
</body>
</html>
