@extends('layout.main')

@section('customcss')
@parent 
@endsection

<script>
    function copyCommand() {
        var copyText = document.getElementById("generatedCommand");

        copyText.select();
        copyText.setSelectionRange(0, 99999); /* For mobile devices */

        navigator.clipboard.writeText(copyText.value);

        alert("Done !");
    }
</script>


@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
            <input type="text" name="generatedCommand" id="generatedCommand" value="{!! \Session::get('success') !!}" style="display: none">
        </ul>
        <button class="btn btn-info" id="copyBranch" onclick="copyCommand()">Copy</button>
    </div>
@endif

@section('content')
    <x-git-branch-generate-form :taskTypes="$taskTypes"/>
    <x-git-legend :taskTypes="$taskTypes"/>
@endsection
