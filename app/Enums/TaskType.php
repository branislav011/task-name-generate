<?php

namespace App\Enums;

enum TaskType: string
{
   case FEATURE = 'feature';
   case BUGFIX = 'bugfix';
   case EPIC = 'epic';
}
