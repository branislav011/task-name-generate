<?php

namespace App\Http\Controllers;

use App\Http\Requests\GitBranchGenerateRequest;
use App\Services\GitBranchGeneratorService;

class GitBranchGenerator extends Controller
{
    /**
     * @var service for git branch generator
     */
    private $service;

    /**
     * @return void
     */
    public function __construct(GitBranchGeneratorService $service)
    {
        $this->service = $service;
    }

    /**
     * Return generator form
     * @return \Illuminate\View\View
     */
    public function showGeneratorForm()
    {
        $taskTypes = $this->service->getTaskTypes();

        return view('git_branch_generator.index', [
            'taskTypes' => $taskTypes,
        ]);
    }

    /**
     * Return generated branch name with or without git command to user
     * @return Illuminate\Http\RedirectResponse
     */
    public function generateBranchName(GitBranchGenerateRequest $request)
    {
        $params = new \StdClass();
        $params->ticketNumber = $this->service->setTicketNumber($request->ticketNumber);
        $params->type = $this->service->setTaskType($request->type);
        $params->command = $this->service->setGitCommand($request->createCommand);
        $params->ticketName = $request->ticketName;

        $branch = $this->service->generateBranch($params);

        //Only if user wanted to create command he can execute it
        if (isset($request->execute) && isset($request->createCommand)) {
            $this->service->createNewBranch($branch);
        }

        return redirect()->back()->with('success', $branch)->withInput($request->except('type'));
    }
}
