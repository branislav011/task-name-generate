<?php

namespace App\Services;

use Illuminate\Support\Str;
use App\Enums\TaskType;
use App\Http\Requests\GitBranchGenerateRequest;

class GitBranchGeneratorService
{
    /**
     * Generate branch name with or without git command
     * @return string
     */
    public function generateBranch(\StdClass $params)
    {
        $ticketNumber = Str::upper($params->ticketNumber);
        $ticketName = Str::kebab($params->ticketName);
        $ticketName = preg_replace('/[^A-Za-z0-9\-\.]/', '', $ticketName);
        $type = Str::lower($params->type);
        $branch = $params->command . $type . '/' . $ticketNumber . '-' . $ticketName;

        return $branch;
    }

    /**
     * Get all task types eg. feature, bugfix... for select box
     * @return Collection
     */
    public function getTaskTypes()
    {
        $cases = TaskType::cases();

        return $cases;
    }

    /**
     * Set type for generating command, default is feature
     * @return string
     */
    public function setTaskType($type)
    {
        $type = $type ?? TaskType::FEATURE->value;

        return $type;
    }

    /**
     * Set ticket number
     * can be also extracted from url
     * @return string
     */
    public function setTicketNumber(string $ticketNumber)
    {
        if (strlen($ticketNumber) > 30) {
            $ticketNumber = explode('selectedIssue=', $ticketNumber)[1];
        }

        return $ticketNumber;
    }

    /**
     * Set git command based on checkbox
     * @return string
     */
    public function setGitCommand($checkbox)
    {
        $command = isset($checkbox) ? 'git checkout -b ' : '';

        return $command;
    }

    /**
     * Create new git branch and checkout
     * @return string
     */
    public function createNewBranch(string $branch)
    {
        $file = base_path() . "\batch\generate_branch.bat";

        exec($file . " \"{$branch}\"", $output);

        return $output;
    }
}
