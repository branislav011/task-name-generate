<?php

namespace App\View\Components;

use Illuminate\View\Component;

class GitBranchGenerateForm extends Component
{
    /**
     * List of task types displayed in select box
     * eg. feature, bugfix...
     */
    public $taskTypes;
    
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($taskTypes)
    {
        $this->taskTypes = $taskTypes;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.git-branch-generate-form');
    }
}
