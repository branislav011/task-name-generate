<?php

namespace Tests\Unit;

use Tests\TestCase;

class GitBranchGeneratorTest extends TestCase
{
    /**
     * Smoke test - if 200 status is returned
     * @return void
     */
    public function test_form_smoke()
    {
        $response = $this->get('/showGeneratorForm');

        $response->assertStatus(200);
    }

    /**
     * Test if user is redirected to form
     * @return void
     */
    public function test_form_redirect_after_visit_homepage()
    {
        $response = $this->get('/');

        $response->assertRedirect('showGeneratorForm');
    }
    
    /**
     * Check if correct form is shown to user
     * @return void
     */
    public function test_form_shown_to_user()
    {
        $response = $this->get('/showGeneratorForm');

        $response->assertSeeInOrder([
            'Ticket number',
            'Ticket name',
            'Feature',
            'Generate Git Command',
            'Generate',
            'Legend',
            'Task types',
        ]);
    }

    /**
     * Check if user cannot submit empty values for ticket number and ticket name
     * @return void
     */
    public function test_form_submit_without_ticket_number_and_ticket_name_return_error()
    {
        $response = $this->post('/generateBranchName', [
            'ticketNumber' => '',
            'ticketName' => '',
        ]);

        $response->assertSessionHasErrors(['ticketNumber', 'ticketName']);
    }

    /**
     * Check if user cannot submit empty value for ticket number
     * @return void
     */
    public function test_form_submit_without_ticket_number_return_error()
    {
        $response = $this->post('/generateBranchName', [
            'ticketNumber' => '',
            'ticketName' => 'Test',
        ]);

        $response->assertSessionHasErrors(['ticketNumber']);
    }

    /**
     * Check if user cannot submit empty value for ticket name
     * @return void
     */
    public function test_form_submit_without_ticket_name_return_error()
    {
        $response = $this->post('/generateBranchName', [
            'ticketNumber' => '205',
            'ticketName' => '',
        ]);

        $response->assertSessionHasErrors(['ticketName']);
    }

    /**
     * Check if bugfix command is generated correctly with git
     * @return void
     */
    public function test_form_submit_generate_git_command_bugfix()
    {
        $response = $this->post('/generateBranchName', [
            'ticketNumber' => 'MP-2233',
            'ticketName' => 'Revert the logging of errors, maybe warn only user code',
            'type' => 'bugfix',
            'createCommand' => true,
        ]);
        $expected = "git checkout -b bugfix/MP-2233-revert-the-logging-of-errors-maybe-warn-only-user-code";

        $response->assertSessionHasAll(['success' => $expected]);
    }

    /**
     * Check if bugfix command is generated correctly without git
     * @return void
     */
    public function test_form_submit_generate__without_git_command_bugfix()
    {
        $response = $this->post('/generateBranchName', [
            'ticketNumber' => 'MP-2233',
            'ticketName' => 'Revert the logging of errors, maybe warn only user code',
            'type' => 'bugfix',
        ]);
        $expected = "bugfix/MP-2233-revert-the-logging-of-errors-maybe-warn-only-user-code";

        $response->assertSessionHasAll(['success' => $expected]);
    }

    /**
     * Check if feature command is generated correctly with git
     * @return void
     */
    public function test_form_submit_generate_git_command_feature()
    {
        $response = $this->post('/generateBranchName', [
            'ticketNumber' => 'MP-2233',
            'ticketName' => 'Revert the logging of errors, maybe warn only user code',
            'type' => 'feature',
            'createCommand' => true,
        ]);
        $expected = "git checkout -b feature/MP-2233-revert-the-logging-of-errors-maybe-warn-only-user-code";

        $response->assertSessionHasAll(['success' => $expected]);
    }

    /**
     * Check if feature command is generated correctly without git
     * @return void
     */
    public function test_form_submit_generate__without_git_command_feature()
    {
        $response = $this->post('/generateBranchName', [
            'ticketNumber' => 'MP-2233',
            'ticketName' => 'Revert the logging of errors, maybe warn only user code',
            'type' => 'feature',
        ]);
        $expected = "feature/MP-2233-revert-the-logging-of-errors-maybe-warn-only-user-code";

        $response->assertSessionHasAll(['success' => $expected]);
    }

    /**
     * Check if epic command is generated correctly with git
     * @return void
     */
    public function test_form_submit_generate_git_command_epic()
    {
        $response = $this->post('/generateBranchName', [
            'ticketNumber' => 'MP-2233',
            'ticketName' => 'Revert the logging of errors, maybe warn only user code',
            'type' => 'epic',
            'createCommand' => true,
        ]);
        $expected = "git checkout -b epic/MP-2233-revert-the-logging-of-errors-maybe-warn-only-user-code";

        $response->assertSessionHasAll(['success' => $expected]);
    }

    /**
     * Check if epic command is generated correctly without git
     * @return void
     */
    public function test_form_submit_generate__without_git_command_epic()
    {
        $response = $this->post('/generateBranchName', [
            'ticketNumber' => 'MP-2233',
            'ticketName' => 'Revert the logging of errors, maybe warn only user code',
            'type' => 'epic',
        ]);
        $expected = "epic/MP-2233-revert-the-logging-of-errors-maybe-warn-only-user-code";

        $response->assertSessionHasAll(['success' => $expected]);
    }
}
