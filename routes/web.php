<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GitBranchGenerator;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('showGeneratorForm', [GitBranchGenerator::class, 'showGeneratorForm']);
Route::post('generateBranchName', [GitBranchGenerator::class, 'generateBranchName']);

Route::get('/', function () {
    return redirect('showGeneratorForm');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
