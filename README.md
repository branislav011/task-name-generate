## About App

The purpose of app is to automate creating git branch following specific convention.

Branch will be created in format *type/number-task-name-in-kebab-case*.

Example: 

```feature/CTI-208-do-some-cool-stuff```

If you leave checkbox **Generate Git Command** it will be:

```git checkout -b feature/CTI-208-do-some-cool-stuff```

If you check **Execute Command** new git branch will be created in your repository (Generate Git Command must be checked also).

---

### Clone and run app :point_left:

- git clone https://branislav011@bitbucket.org/branislav011/task-name-generate.git
- composer install
- npm install
- npm run dev
- php artisan serve

Available at [localhost:8000]([http://localhost:8000) (if port is not in use).

### Auto run on startup (Windows) :point_left:

1. Press Win key + R
2. Type shell:startup
3. Copy script git_generator.bat from batch folder
4. Project will be started and opened in browser :relaxed:

If you expected from app to open project automatically in Chrome, you must set Chrome as default browser. 

Instructions:

[Google Chrome as default browser](https://support.google.com/chrome/answer/95417?hl=en&co=GENIE.Platform%3DDesktop#zippy=%2Cwindows)

### Tech Stack

- Laravel 9.2
- PHP 8.1